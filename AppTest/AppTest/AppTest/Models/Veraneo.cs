﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTest.Models
{
    [DataTable("Veraneos")]
    public class Veraneo
    {
        public string Id { get; set; }
        public string Rol { get; set; }
        public string RazonSocial { get; set; }
        public string Rubro { get; set; }
        public string ImpEconomica { get; set; }
        public string Region { get; set; }
        public string Situacion { get; set; }
        public string Estado { get; set; }
        [Version]
        public string Version { get; set; }
    }
}
