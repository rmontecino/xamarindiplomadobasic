﻿using AppTest.Models;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTest.Services
{
    public class AzureService
    {
        #region Atributos

        IMobileServiceClient Client;
        IMobileServiceTable<Veraneo> Veraneos;

        #endregion

        #region Constructor

        public AzureService()
        {
            string _serviceUrl = "http://diplomadoxamarinrm.azurewebsites.net";
            Client = new MobileServiceClient(_serviceUrl);
            Veraneos = Client.GetTable<Veraneo>();
        }

        #endregion

        #region Métodos

        public Task<IEnumerable<Veraneo>> GetVeraneos()
        {
            return Veraneos.ToEnumerableAsync();
        }

        #endregion
    }
}
