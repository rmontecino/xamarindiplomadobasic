﻿using AppTest.Models;
using AppTest.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppTest.ViewModels
{
    class VeraneosViewModel : INotifyPropertyChanged
    {
        #region Atributos

        public ObservableCollection<Veraneo> Veraneos { get; set; }
        public Command GetVeraneosCommand { get; set; }
        private bool Busy;

        #endregion

        #region Propiedades

        public bool IsBusy
        {
            get
            {
                return Busy;
            }

            set
            {
                Busy = value;
            }
        }

        #endregion

        #region Constructor

        public VeraneosViewModel()
        {
            Veraneos = new ObservableCollection<Veraneo>();
            GetVeraneosCommand = new Command(
                async () => await GetVeraneos(),
                      () => !IsBusy);
        }

        #endregion

        #region Eventos

        void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Métodos

        private async Task GetVeraneos()
        {
            if (!IsBusy)
            {
                Exception Error = null;
                try
                {
                    IsBusy = true;
                    var Service = new AzureService();
                    var Items = await Service.GetVeraneos();
                    Veraneos.Clear();
                    foreach(var Item in Items)
                    {
                        Veraneos.Add(Item);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"Error: {ex}");
                    Error = ex;
                }
                finally
                {
                    IsBusy = false;
                }

                if (Error != null)
                {
                    await Application.Current.MainPage.DisplayAlert("Error!", Error.Message, "OK");
                }
                return;
            }
        }

        #endregion
    }
}
